# The Boardgame Club Homepage

Welcome to the readme of our homepage!  If you are seeing this, it means you are on the git repo, or beaker source code, of the site which means yr a _seeker_ and we are all about seekers.

And, if you are in Wellington and a part of our boardgaming club _and_ seeking out the source for the site, than we want you as a collaborator too!  Please, make this site yr owna nd expand it as you see fit!

## Things you can do to this site:
- Customize the CSS and aesthetic
- Add your own subdirectory and llink to it from the main page, and then make that directory your home within this home.
- Add features and details to our home or other sites.
- Add stories and tutorials to the readme's to better help the next person.
- Something that you just came up with and aren't seeing here.

ALL of it is welcome!  As you make and push changes, they'll automatically show on the website.  So i'd say 'be careful', but there's like 10 people checking out the site and they're all friends so break everythign, it won't matter! Have fun!

## How to contribute
First some admin prelims:
- Make an account on gitlab (it is free, and you can sign up with just your email)
- Ping me (Zach Mandevlle) and ask to be a maintainer of this site (I'll say yes, no worries!)

Then, to the funs stuff:
- Clone this repo to your own computer with `git clone shdjsjdjdjaj `
- navigate into the directory `cd bgc`
- make whatever changes you want to make.
- add those changes with `git add -A`
- commit them with `git commit -m "message about what you did"`
- push them up with `git push`

After the first time, you'll want to make sure you're working on the most recent version of our site, and not stepping on anyone else's toes.  You can do this by pulling in new changes before pushing in your own.  And so the flow would be:
- git pull
- do your magic
- git add -A
- git commit -m 'message'
- git push

## Other Things to Check out Here
Our calendar page!  I've made a lil static calendar generator...and you can join our collective calendar!

Our Aesthetic page.
Our boardgame pages.


- 

